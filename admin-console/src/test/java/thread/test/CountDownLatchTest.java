package thread.test;


import java.util.concurrent.CountDownLatch;

/**
 *  主线程等待子线程执行完毕
 */
public class CountDownLatchTest {



    public static void main(String[] args) throws InterruptedException {
        //1、 创建CountDownLatch 对象， 设定需要计数的子线程数目
        final CountDownLatch latch=new CountDownLatch(3);
        System.out.println("主线程开始执行....");
        for (int i = 0; i < 3; i++) {
            new Thread(){
                @Override
                public void run() {
                    try {
                        System.out.println(Thread.currentThread().getName()+"  开始执行存储过程..");
                        Thread.sleep(2000);
                        System.out.println(Thread.currentThread().getName()+"  存储过程执行完毕...");
                        //2、子线程执行完毕，计数减1
                        latch.countDown();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                };
            }.start();
        }
        System.out.println("等待子线程执行完毕...");
        //3、 当前线程挂起等待
        latch.await();
        System.out.println("主线程执行完毕....");
    }
}
