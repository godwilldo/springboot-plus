package springboot;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * spring boot 功能测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootDemoTest {


    @Before
    public void initBefore(){
        System.out.println("init");
    }

}
