package algorithm;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

/**
 * 框架主函数
 * 
 * @author zhangyue
 * @date 2019年3月4日 下午5:32:34
 */
public class MainFrame {

    private static BufferedWriter bw = null;
    private static final char DOT = ',';
    private static final char NEWLINE = '\n';    

    public static void main(String[] args) throws Exception {
        if (!argsCheck(args)) {
            System.out.println("params not match: " + Arrays.toString(args)
                    + ", need inputfile outputfile tempdir!");
            System.exit(0);
        }

        final String inputFile = args[0];// 输入文件
        final String outputFile = args[1];// 输出文件
        final String useTimeFile = args[2];// 运行用时
        final String tempDir = args[3];// 计算用临时目录

        long totalTime = 0;

        try {

            bw = Files.newBufferedWriter(Paths.get(outputFile), StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);

            Solution solution = new Solution();

            // 初始化
            solution.init(tempDir);

            // 执行主体
            long startTime = System.nanoTime();
            solution.process(inputFile);
            long endTime = System.nanoTime();

            // 计时
            totalTime = (endTime - startTime) / 1000;
            Files.write(Paths.get(useTimeFile), String.valueOf(totalTime).getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        }finally {
            closeWriter(bw);
        }

        System.exit(0);

    }

    private static boolean argsCheck(String[] str) {
        return str == null || str.length == 4;
    }

    public static void addSet(final String[] result) throws Exception {
        int size = result.length;
        bw.write(result[0]);
        for (int i = 1; i < size; i++) {
            bw.write(DOT);
            bw.write(result[i]);
        }
        bw.write(NEWLINE);
    }

    private static void closeWriter(BufferedWriter bw) throws IOException {
        if (bw != null) {
            bw.close();
        }
    }

}