package algorithm;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * 请在此类中完成解决方案，需要实现init完成数据初始化，实现process完成数据的处理逻辑。
 * 
 * @author zhangyue
 * @date 2019年3月4日 下午3:38:00
 */
public class Solution {

    /**
     * 初始化
     * 
     * @param tempDir 可读写的临时目录
     */
    public void init(String tempDir) throws Exception {
        // 此部分不计入执行用时，提供了临时目录用于暂存数据，请合理发挥。
    }

    /**
     * 主体逻辑实现demo，实现代码时请注意逻辑严谨性，涉及到操作文件时，保证文件有开有闭等。
     * 
     * @param inputPath 输入文件路径，请不要hack它-_-。
     */
    public void process(String inputPath) throws Exception {
        Map<String,Set<String>> connectMap = new HashMap<>(100000,0.75f);
        Map<String,String> dataMap = new HashMap<>(100000,0.75f);
        //创建初始化节点和子节点数目
        try(BufferedReader reader = Files.newBufferedReader(Paths.get(inputPath))) {
            int idx=0;
            for (;;) {
                String line = reader.readLine();
                if(line == null){
                    break;
                }
                String[] strs = line.split(",");
                for(int i =0;i<strs.length;i++){
                    if(!dataMap.containsKey(strs[i])){
                        dataMap.put(strs[i],strs[i]);
                        Set<String> set = new HashSet<>();
                        set.add(strs[i]);
                        connectMap.put(strs[i],set);
                        idx++;
                    }
                }
                String root0 = strs[0];
                while(!root0.equals(dataMap.get(root0))){
                    root0=dataMap.get(root0);
                }
                String root1 = strs[1];
                while(!root1.equals(dataMap.get(root1))){
                    root1=dataMap.get(root1);
                }
                Iterator<String> it=null;
                if(!root1.equals(root0)){
                    if(connectMap.get(root0).size()>connectMap.get(root1).size()){
                         it= connectMap.get(root1).iterator();
                        while(it.hasNext()){
                            connectMap.get(root0).add(it.next());
                        }
                        dataMap.put(root1,root0);
                        connectMap.put(root1,null);
                    }else{
                        it = connectMap.get(root0).iterator();
                        while(it.hasNext()){
                            connectMap.get(root1).add(it.next());
                        }
                        dataMap.put(root0,root1);
                        connectMap.put(root0,null);
                    }
                }
            }
            for(Map.Entry<String,Set<String>> v1: connectMap.entrySet()) {
                if(v1.getValue()!= null){
                    MainFrame.addSet(v1.getValue().toArray(new String[v1.getValue().size()]));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
